# Component - Rust Basic

This component provides a basic template to build and test Rust projects, including best practices such as caching.

```
templates/
  rust-basic.yml
README.md 
```

It is the first CI/CD component project created by @dnsmichi -- you can inspect the Git history to learn. Treat this project with the required awareness -- it is far from perfect, and will be released in learning iterations.

This is a demo project, and not meant for production usage.

## Usage

**Note** This example is will be updated while more features are added in the learning process. 

```
stages: 
  - build
  - test

include:
  - component: gitlab.com/gitlab-de/use-cases/cicd-components-catalog/components-templates/rust-basic/rust-basic@main
      inputs:
        stage_build: build
        stage_test: test 
        rust_version: latest

  - component: gitlab.com/gitlab-de/use-cases/cicd-components-catalog/components-templates/rust-basic/rust-basic@main
      inputs:
        rust_version: 1.72.0        

```

You can also specify tagged releases instead of `main`, which defaults to the latest HEAD in the default branch.

- [0.1](https://gitlab.com/gitlab-de/use-cases/cicd-components-catalog/components-templates/rust-basic/-/releases/0.1) is the MVC working version.
- [0.2](https://gitlab.com/gitlab-de/use-cases/cicd-components-catalog/components-templates/rust-basic/-/releases/0.2) adds job templates.
- [0.3](https://gitlab.com/gitlab-de/use-cases/cicd-components-catalog/components-templates/rust-basic/-/releases/0.3) adds support for Rust caching. 
- [0.4](https://gitlab.com/gitlab-de/use-cases/cicd-components-catalog/components-templates/rust-basic/-/releases/0.4) adds support for inputs: `stage_build` and `stage_test`. 
- [0.5](https://gitlab.com/gitlab-de/use-cases/cicd-components-catalog/components-templates/rust-basic/-/releases/0.5) adds support for inputs: `rust_version` to specify the image tag, and dynamically generate CI/CD names based on the version. 

You include multiple components with different versions, and inputs. 

## Tests

The CI/CD component is tested in [.gitlab-ci.yml](.gitlab-ci.yml). The jobs run `cargo` which requires

1. `Cargo.toml` as project configuration
2. Sample Rust code in `src/main.rs`

## Resources

- Consumer demo project in https://gitlab.com/gitlab-de/use-cases/cicd-components-catalog/consumers/rust-hello-component 